//
//  NoteTest.swift
//  NotesappTests
//
//  Created by zopper on 16/04/18.
//  Copyright © 2018 sachin. All rights reserved.
//

import XCTest
@testable import Notesapp

class NoteTest: XCTestCase {
    
    var noteRight:Note!
    var noteWrong:Note!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        noteRight = Note(header: "Right Header", notedescription: "This text is less than 300 characters")
        noteWrong = Note(header: "Wrong Header", notedescription: "This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. This text is greater than 300 characters. ")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        noteRight = nil
        noteWrong = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    func testForLengthOfDescription(){
       XCTAssertTrue(noteRight.description.count <= 300)
        XCTAssertFalse(noteWrong.description.count <= 300)
    }
    
}
