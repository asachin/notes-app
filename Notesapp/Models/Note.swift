//
//  Note.swift
//  Notesapp
//
//  Created by zopper on 15/04/18.
//  Copyright © 2018 sachin. All rights reserved.
//

import Foundation
import RealmSwift

class Note:Object {
    @objc dynamic var header:String = ""
    @objc dynamic var notedescription:String = ""
    
    convenience init(header:String, notedescription:String) {
        self.init()
        self.header = header
        self.notedescription = notedescription
    }
}


