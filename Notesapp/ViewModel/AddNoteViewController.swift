//
//  AddNoteViewController.swift
//  Notesapp
//
//  Created by zopper on 16/04/18.
//  Copyright © 2018 sachin. All rights reserved.
//

import UIKit
import RxSwift

class AddNoteViewController: UIViewController {
    @IBOutlet weak var headerTextView: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    var note:Note?
    @IBOutlet weak var charactersLeftLabel: UILabel!
    var disposeBag = DisposeBag()
    
    let MAX_COUNT = 300
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "AddNoteViewController"
        descriptionTextView.delegate = self
        descriptionTextView.rx.text.map{ "\(self.MAX_COUNT - ($0 ?? "").count) characters left" }.bind(to: charactersLeftLabel.rx.text).disposed(by: disposeBag)
        if let noteObject = note {
            headerTextView.text = noteObject.header
            descriptionTextView.text = noteObject.notedescription
        }

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        let borderColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
        
        descriptionTextView.layer.borderColor = borderColor.cgColor;
        descriptionTextView.layer.borderWidth = 1.0;
        descriptionTextView.layer.cornerRadius = 5.0;
        if(note == nil){
            self.navigationItem.rightBarButtonItems = nil
            self.navigationItem.setRightBarButton(saveButton, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func deleteNote(_ sender: Any) {
        RealmService.shared.delete(note!)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func saveNote(_ sender: Any) {
        if let noteObject = note {
            let dict:[String:String] = [
                "header":headerTextView.text ?? "",
                "notedescription":descriptionTextView.text
            ]
            RealmService.shared.update(noteObject, with: dict)
        }else{
           let addNoteObject = Note(header: headerTextView.text ?? "", notedescription: descriptionTextView.text)
        RealmService.shared.create(addNoteObject)
        }
        navigationController?.popViewController(animated: true)
    }
}

extension AddNoteViewController:UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.count - range.length + text.count <= 300
    }
}
