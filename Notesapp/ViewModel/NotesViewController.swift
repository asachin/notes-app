//
//  NotesViewController.swift
//  Notesapp
//
//  Created by zopper on 15/04/18.
//  Copyright © 2018 sachin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm

class NotesViewController: UIViewController {
     @IBOutlet private var tableView: UITableView!
//    let notes:Variable<[Note]> = Variable([])
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCellConfiguration()
        setupSelectNote()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func addNote(_ sender: UIBarButtonItem) {
        let addNoteViewController = storyboard?.instantiateViewController(withIdentifier: "AddNoteViewController") as! AddNoteViewController
        navigationController?.pushViewController(addNoteViewController, animated: true)
        
    }
    private func setupCellConfiguration() {
        let notes = RealmService.shared.realm.objects(Note.self)
        Observable.array(from: notes)
            .bind(to: tableView.rx.items(cellIdentifier: "NotesTableViewCell", cellType: NotesTableViewCell.self)) { (row, element, cell) in
                cell.configureWithNote(note: element)
            }
            .disposed(by: disposeBag)
    }
    private func setupSelectNote(){
        tableView.rx.modelSelected(Note.self).subscribe(onNext: {
            [weak self] note in
            let addNoteViewController = self?.storyboard?.instantiateViewController(withIdentifier: "AddNoteViewController") as! AddNoteViewController
            addNoteViewController.note = note
            self?.navigationController?.pushViewController(addNoteViewController, animated: true)
            if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
                self?.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
            }
        }).disposed(by: disposeBag)
    }
    
}
