//
//  NotesTableViewCell.swift
//  Notesapp
//
//  Created by zopper on 15/04/18.
//  Copyright © 2018 sachin. All rights reserved.
//

import UIKit

class NotesTableViewCell: UITableViewCell {
    static let Identifier = "NotesTableViewCell"
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configureWithNote(note:Note){
        headerLabel.text = note.header
        descriptionLabel.text = note.notedescription
    }
}
