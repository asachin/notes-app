//
//  RealmService.swift
//  Notesapp
//
//  Created by zopper on 16/04/18.
//  Copyright © 2018 sachin. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm

class RealmService {
    public init() {
        
    }
    static let shared = RealmService()
    
    var realm = try! Realm()
    
    func create<T:Object>(_ object:T){
        do{
            try realm.write {
                realm.add(object)
            }
        } catch {
            print(error)
        }
    }
    func update<T:Object>(_ object:T,with dictionary:[String:Any?]){
        do{
            try realm.write {
                for (key,value) in dictionary{
                    object.setValue(value, forKey: key)
                }
            }
        } catch {
            print(error)
        }
    }
    
    func delete<T:Object>(_ object:T){
        do{
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print(error)
        }
    }
}
